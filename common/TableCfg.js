module.exports = {
    ZK_PERMITINFO: {
        name: "ZK_PERMITINFO",
        pk: "ZK_ID"
    },
    ZK_PERMITCONFIG: {
        name: "ZK_PERMITCONFIG",
        pk: "ZK_ID"
    },
    ZK_USERINFO: {
        name: "ZK_USERINFO",
        pk: "ZK_ID"
    },
    ZK_ROLEINFO: {
        name: "ZK_ROLEINFO",
        pk: "ZK_ID"
    },
    ZK_PARAMINFO: {
        name: "ZK_PARAMINFO",
        pk: "ZK_ID"
    },
    ZK_INVESTMENT: {
        name: "ZK_INVESTMENT",
        pk: "ZK_ID"
    },
    ZK_SENSORDATA: {
        name: "ZK_SENSORDATA",
        pk: "ZK_ID"
    }
};