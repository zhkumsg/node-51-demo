//统一封装返回
module.exports = {
    ZK_INVESTMENT: require('./ZK_INVESTMENT'),
    ZK_PARAMINFO: require('./ZK_PARAMINFO'),
    ZK_PERMITCONFIG: require('./ZK_PERMITCONFIG'),
    ZK_PERMITINFO: require('./ZK_PERMITINFO'),
    ZK_ROLEINFO: require('./ZK_ROLEINFO'),
    ZK_USERINFO: require('./ZK_USERINFO')
}