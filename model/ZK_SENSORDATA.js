//定义ZK_ROLEINFO实体类
const propertys = [
    "ZK_ID",
    "ZK_HUMI",
    "ZK_TEMP",
    "ZK_ILLU",
    "ZK_TIME",
    "EB_ISDELETE",
    "EB_CREATE_DATETIME",
    "EB_CREATEBY",
    "EB_LASTMODIFYBY",
    "EB_LASTMODIFY_DATETIME"
];

class ZK_SENSORDATA {
    constructor() {
        propertys.forEach(name => { this[name] = undefined; });
        this.set(arguments[0]);
    }
    get(key) {
        return this[key];
    }
    set() {
        if (typeof arguments[0] === "object") {
            for (let name in arguments[0]) {
                if (propertys.indexOf(name) > -1) {
                    this[name] = arguments[0][name];
                }
            }
        }
    }
}


module.exports = ZK_SENSORDATA;