# node-51-demo

#### 项目介绍
基于node-base-demo，作为web服务后台，连接数据库、终端采集系统和web前端

#### 软件架构
nodejs、express、sql server 2008r2


#### 安装教程

1. npm install
2. npm run dev

#### 使用说明

1. [手摸手，教你用nodejs搭建后台最小系统（大量图文）系列一](https://www.jianshu.com/p/ebef9ffb7851)
2. [手摸手，教你用nodejs搭建后台最小系统（大量图文）系列二](https://www.jianshu.com/p/60d6dc2d901a)