const client = require('./common/ServiceClient');
const QueryModel = require('./common/QueryModel');
const C51Helper = require('./common/C51Helper');

//global
const Webglobal = {

    /**
     * 启动项目时调用
     */
    start() {
        global["SYS_PARAMINFO"] = [];
        client.Query(QueryModel.ZK_PARAMINFO, [], null, 0, 0, false, null).then(m => {
            global["SYS_PARAMINFO"] = m.result;
        }).catch(err => {
            console.log("获取系统参数异常", err);
        });

        //启动C51实例
        global["SYS_C51"] = new C51Helper();
    }

};

module.exports = Webglobal;