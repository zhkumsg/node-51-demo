module.exports = {
    DbConnectionString: "mssql://sa:123456@127.0.0.1:1433/C51Demo",
    MySqlConnectionCfg: {
        host: '*****',
        user: 'root',
        password: '*****',
        port: '3306',
        database: '*****'
    },
    TokenSecret: "msg is a handsome boy",
    HttpPort: 3000,
    WebsocketPort: 5000,
    C51cfg: {
        wsPort: 5000,
        comName: "COM5",
        baudRate: 4800,
        upload: false
    }
};